FROM python:3.8-slim as base

# TODO: Packages blocking from: --use-feature=2020-resolver parameter for pip:
# - python-keyczar


# Requirements rationale:
#
# onelogin: libxml2 and libxmlsec1
# mysqlclient: default-libmysqlclient-dev
# git: installation of git+ packages from requirements.txt
# wkhtmltopdf: PDF rendering with pdfkit

# NOTE: all those packages will go into live image.
RUN apt-get update \
    && apt-get install -y \
    default-libmysqlclient-dev \
    nginx \
    wget \
    neovim \
    libxml2-dev \
    libxmlsec1-dev \
    git  \
    && python3 -m pip install --upgrade pip \
    && wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.stretch_amd64.deb \
    && apt install -y ./wkhtmltox_0.12.5-1.stretch_amd64.deb \
    && rm wkhtmltox_0.12.5-1.stretch_amd64.deb

# ----------------------------
# 1. Python dependencies build stage
# ----------------------------
FROM base as builder

# NOTE: those packages will only be used in building phase.
RUN apt-get update \
    && apt-get install -y \
    autoconf \
    automake \
    build-essential \
    gcc \
    git \
    libssl-dev \
    libtool \
    make \
    pkg-config \
    python-dev \
    wget \
    && rm -rf /var/lib/apt/lists/*


# Install python requirements.
WORKDIR /wheels
COPY ./requirements.txt /wheels/requirements.txt
RUN pip3 wheel --use-deprecated=legacy-resolver -r requirements.txt

# ----------------------------
# 2. Construct final image.
# ----------------------------
FROM base

ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

COPY --from=builder /wheels /wheels
COPY ./vendor/ /wheels/vendor
COPY ./misc/GeoIP.dat /usr/local/geoIP/GeoIP.dat
COPY misc/ken-nginx.conf /etc/nginx/sites-available/ken-proxy

RUN pip3 install -r /wheels/requirements.txt -f /wheels  --use-deprecated=legacy-resolver --use-feature=fast-deps\
    && pip3 install /wheels/vendor/* \
    && rm /etc/nginx/sites-enabled/default  \
    && ln -s /etc/nginx/sites-available/ken-proxy /etc/nginx/sites-enabled/ken-proxy \
    && rm -rf /wheels \
    && rm -rf /root/.cache \
    && rm -rf /var/lib/apt/lists/* \
    && cp /etc/ssl/certs/ca-certificates.crt `python3 -c 'import certifi; print(certifi.where())'`

COPY . /var/www/html

WORKDIR /var/www/html
