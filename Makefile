MAIN_CONTAINER_NAME=ken

# Entry points {{{
build: build_backend
exec: bash
up: run
rebuild: build run

logs:
	docker logs -f $(MAIN_CONTAINER_NAME)

restart:
	docker restart $(MAIN_CONTAINER_NAME)
bash:
	docker exec -it $(MAIN_CONTAINER_NAME) bash
shell:
	docker exec -it $(MAIN_CONTAINER_NAME) bash -c './manage.py shell_plus'
attach:
	docker attach $(MAIN_CONTAINER_NAME)
build_backend: cleanup
	cd ../../.. && make build
build_backend_no_cache: cleanup
	cd ../../.. && make build_no_cache
run: fix_elasticsearch
	cd ../../.. && docker-compose up -d --remove-orphans &
run_elk: fix_elasticsearch
	cd ../../.. && docker-compose -f docker-compose.yml  -f docker-compose-elk.yml up -d --remove-orphans &
run_all: fix_elasticsearch
	sudo rm -f celerybeat*
	cd ../../.. && docker-compose -f docker-compose.yml  -f docker-compose-celery.yml up -d --remove-orphans &
run_celery:
	docker kill celery_beat || echo 'No beat found'
	docker kill celery_flower || echo 'No flower found'
	cd ../../.. && docker-compose -f docker-compose.yml  -f docker-compose-celery.yml up -d  celery &
down:
	cd ../../.. && docker-compose -f docker-compose.yml  -f docker-compose-celery.yml -f docker-compose-elk.yml down
fix_cross_device:
	# https://gist.github.com/Francesco149/ce376cd83d42774ed39d34816b9e21db
	echo N | sudo tee /sys/module/overlay/parameters/metacopy
fix_elasticsearch:
	sudo bash -c "sysctl -w vm.max_map_count=262144" || echo "NOTE: Wasn't able to fix ES"
# }}}
#  Testing {{{
# This command is used on CI as well.
test:
	docker exec -it $(MAIN_CONTAINER_NAME) pytest --create-db --runslow --mysql
test_coverage:
	docker exec -it $(MAIN_CONTAINER_NAME) pytest --create-db --runslow --mysql --cov=. --cov-report=xml --junitxml=test-reports/junit.xml
typing:
	docker exec $(MAIN_CONTAINER_NAME) mypy --config-file .mypy.ini .
# NOTE: Currently in development and has a lot of warnings.
typing_strict:
	docker exec $(MAIN_CONTAINER_NAME) mypy --strict --config-file .mypy.ini libs/
# }}}
# Maintenance {{{
cleanup:
	find . | grep -E "(__pycache__|\.pyc|\.pyo$$)" | xargs sudo rm -rf
static:
	docker exec -it $(MAIN_CONTAINER_NAME) bash -c './manage.py collectstatic -l --noinput'
migrate:
	docker exec -it $(MAIN_CONTAINER_NAME) bash -c './manage.py migrate'
	docker exec -it $(MAIN_CONTAINER_NAME) bash -c './manage.py migrate --database=cloudtrail_db'

ingest:
	docker exec -it $(MAIN_CONTAINER_NAME) bash -c 'cd /var/www/html; DJANGO_SETTINGS_MODULE=ken.settings.production python -m misc.bootstrap_local'

wipe_volumes:
	docker volume rm `docker volume ls -q | grep ecs-refarch`

# }}}
# vim:foldmethod=marker:foldlevel=1
