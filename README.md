# MAP_2020_Team11

## Team

- Egor Baklanov (@EgorBak)

## Description

nOps - cloud management tool for next-gen MSPs & complex organizations.

Using nOps you can Monitor, analyze, and optimize AWS changes, cost, security, performance, reliability, and operational excellence. Built for fast-moving DevOps teams.

## Project management

- BitBucket - code hosting, review tool.
- Jira - backlog, current epics and issue tracking.
- Slack, Telegram - communications.
- https://www.planningpoker.com/ - Points estimation.

## Technological stack

- Python 3.8 - BackEnd
- Django 2.2.16, DRF 3.9.4 - BackEnd frameworks
- Celery + Redis - Tasks queue
- Pytest - Testing
- loguru - Logging
- ElasticSearch (ELK in fact) - nonRelational DB
- Postgres, MySql - Relational DB
- Docker
- k8s (WIP)
- React - FrontEnd framework
- Angular (deprecated) - FrontEnd framework
- CircleCi - CI
- AWS CloudFormation - CD

## Process management

- Daily stand-up - 6 PM
- Planning - Friday 6 PM, 6:30 PM (another team)
- Backlog Grooming meeting - Tuesday 6:30 PM

# Setup guide

#### System Dependencies

- docker
- docker-compose
- git
- make

### Installation and testing

#### Init

Setup (and obtain) your credentials to nclouds account
```
export AWS_ACCESS_KEY_ID=
export AWS_SECRET_ACCESS_KEY=
export AWS_DEFAULT_REGION=us-west-2

```

Then:
```
# Clone repo with dockerfiles
git clone git@bitbucket.org:nclouds/ecs-refarch-cloudformation.git
# Clone source
git clone git@bitbucket.org:nclouds/ken.git ecs-refarch-cloudformation/services/ken/src

cd ecs-refarch-cloudformation/services/ken/src
make
make run
docker exec -it ken ./manage.py migrate --database=cloudtrail_db
docker exec -it ken ./manage.py migrate
```

#### Testing

```
make test
```

#### Getting data

This call might take a few hours to complete. For details refer to `misc/bootstrap_local.py` script which contains every ingestion function.
In order to ingest everything invoke:

```
make ingest
```

For additional info refer to Makefile and https://bitbucket.org/nclouds/ecs-refarch-cloudformation



#### Setting up frontend

```
git clone git@bitbucket.org:nclouds/ken_frontend.git
```

Modify `package.json` in ken_frontend folder to reflect your local ip (`http://localhost:8080`)

Verify your email in django shell: `EmailAddress.objects.all().update(verified=True)`

```
cd ken_frontend

yarn install
yarn start
```

Go to: http://localhost:3000/

Default login credentials for local setup:
```
localdev@test.com
RbL1mUAHckyUs8knMGr1ppG4lrtQQoMzK81wm
```

**You're beautiful.**


# Development guide:

Code is always pushed to separate branch, after that PR should be raised to merge it into master.
Once in master CircleCI will deploy your code to uat.nops.io
To deploy to production one should push to `deploy` branch.

Code format (follow it):
```
isort --sl -m 3
black
```


# Flow details

#### Required Ubuntu dependencies

Those dependencies are required to build docker image, you don't have to install them on your host.

    sudo apt-get install libmysqlclient-dev   # for mysql_config
    sudo apt-get install libxmlsec1-dev pkg-config   # for xmlsec


### Current deployment structure for HTTP request

#### AWS
- Main nops.io is hosted with Route 53
- CloudFront is used to serve SSL and requests to application with 2 targets
- ECS instances are registered with ELB target automatically

#### Backend
- Request to backend would go to ELB -> ECS -> Ken Instances
- In Ken instance nginx would listen on 80 port and forward it to uWSGI instance which is running in 8000 port.
- URL are declared in app/urls.py and ken/urls.py
- Any request to URL with prefix v2/ would be served as Angular APP (Deprecated).
- Any request to v3/ and partner/ would be served as React App.
- Angular Index file and React Index file is a simple HTML file would load Javascript application.
- The html file would be served by a simple proxy to fetch HTML from S3 static bucket (the same as Static file). The reason we should use this proxy because we need to embed contexts (Client name client ID).
- We might improve this flow later by get context out of Template file and load it via HTTP request.

